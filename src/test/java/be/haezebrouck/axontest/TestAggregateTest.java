package be.haezebrouck.axontest;

import be.haezebrouck.axontest.testaggregate.Create;
import be.haezebrouck.axontest.testaggregate.Created;
import be.haezebrouck.axontest.testaggregate.TestAggregate;
import be.haezebrouck.axontest.testaggregate.Update;
import be.haezebrouck.axontest.testaggregate.Updated;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.junit.Test;

import java.util.UUID;

public class TestAggregateTest {

    public static final String ID = UUID.randomUUID().toString();

    private AggregateTestFixture<TestAggregate> fixture =
            new AggregateTestFixture<>(TestAggregate.class);

    @Test
    public void create_createdEvent() {
        fixture.givenNoPriorActivity()
                .when(new Create(ID))
                .expectEvents(new Created(ID));
    }

    @Test
    public void update_n_createNEvents() {
        fixture.given(new Created(ID))
                .when(new Update(ID, 3))
                .expectEvents(
                        new Updated(ID, 0),
                        new Updated(ID, 1),
                        new Updated(ID, 2));
    }
}
