package be.haezebrouck.axontest;

import be.haezebrouck.axontest.JsonUtil;
import be.haezebrouck.axontest.testaggregate.TestAggregate;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class TestAggregateSerializationTest {

    @Test
    public void serialize_deserialize_shouldBeTheSame() throws NoSuchFieldException, IllegalAccessException {
        TestAggregate original = new TestAggregate();
        String id = UUID.randomUUID().toString();

        aggregateField("id").set(original, id);
        aggregateField("eventCounter").set(original, 42);

        String serialized = JsonUtil.object2json(original);
        TestAggregate deserialized = JsonUtil.json2object(serialized, TestAggregate.class);

        assertThat(aggregateField("id").get(deserialized))
                .isEqualTo(id);
        assertThat(aggregateField("eventCounter").get(deserialized))
                .isEqualTo(42);

    }

    private Field aggregateField(String field) throws NoSuchFieldException {
        Field f = TestAggregate.class.getDeclaredField(field);
        f.setAccessible(true);
        return f;
    }

}
