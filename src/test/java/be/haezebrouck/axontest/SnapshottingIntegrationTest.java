package be.haezebrouck.axontest;

import be.haezebrouck.axontest.testaggregate.Create;
import be.haezebrouck.axontest.testaggregate.TestAggregate;
import be.haezebrouck.axontest.testaggregate.Update;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.lang.reflect.Field;
import java.util.Map;

import static be.haezebrouck.axontest.JsonUtil.json2object;
import static java.lang.Integer.parseInt;
import static org.assertj.core.api.Assertions.assertThat;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SnapshottingIntegrationTest {

    @Autowired
    private AxonConfig axonConfig;

    @Autowired
    protected CommandGateway commandGateway;

    @Value("${axon.axonserver.url.snapshotquery}")
    private String snapshotQueryUrl;

    @Test
    public void snapshot_shouldBeSerializedInReadableJson() throws Exception {
        assertThat(axonConfig.snapshotThreshold() > 2).isTrue();

        String id = givenAggregateCreated();


        for(int i = 0; i < axonConfig.snapshotThreshold() +10 ; i++) {
            additionalCommand(id);
        }

        String serializedPayload = getSnapshotPayload(id);

        TestAggregate deserializedPayload = json2object(serializedPayload, TestAggregate.class);
        assertThat(getInternalState(deserializedPayload, "id" )).isEqualTo(id);
        assertThat(parseInt(getInternalState(deserializedPayload, "eventCounter").toString()))
                .isGreaterThanOrEqualTo(axonConfig.snapshotThreshold());
        getLogger(getClass()).info("id={}", id);
    }

    private void additionalCommand(String id) {
        commandGateway.sendAndWait(new Update(id, 1));
    }


    private Object  getInternalState(Object target, String field) throws Exception {
        Field f = target.getClass().getDeclaredField(field);
        f.setAccessible(true);
        return f.get(target);
    }

    private String givenAggregateCreated() {
        Create create = new Create();
        commandGateway.sendAndWait(create);
        return create.id;
    }

    protected String getSnapshotPayload(String aggregateId) {
        Map<String, Map<String, String>> event = queryEvents(snapshotQueryUrl,"aggregateId="+aggregateId)
                .filter(e->e.get("payload") != null
                        && TestAggregate.class.getName().equals(e.get("payload").get("type")))
                .blockFirst();
        return event.get("payload").get("data");
    }


    private Flux<Map<String, Map<String,String>>> queryEvents(String queryUrl, String query) {
        WebClient webClient = WebClient.create();
        ParameterizedTypeReference<ServerSentEvent<Map<String, Map<String, String>>>> type = ParameterizedTypeReference.forType(ServerSentEvent.class);

        String uriString = fromHttpUrl(queryUrl).encode().query(query).toUriString();

        return
                webClient
                        .get()
                        .uri(uriString)
                        .accept(MediaType.TEXT_EVENT_STREAM)
                        .retrieve().bodyToFlux(type).map(event->event.data());
    }

}
