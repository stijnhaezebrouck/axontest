package be.haezebrouck.classtypeserialization;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Optional;

public class Response<T> {
    @JsonProperty("data")
    private T data;
    private double executionDuration;
    private boolean success;
    private String version;

    public Response() {}

    public Response(Class<T> dataClass, T data, double executionDuration, boolean success, String version) {
        this.dataClass = dataClass;
        this.data = data;
        this.executionDuration = executionDuration;
        this.success = success;
        this.version = version;
    }

    //HOW TO Make Jackson to inject this?
    private Class<T> dataClass;

    @JsonIgnore
    public Optional<T> getData() {
        return Optional.ofNullable(data);
    }

    public double getExecutionDuration() {
        return executionDuration;
    }

    public Class<T> getDataClass() {
        return dataClass;
    }

    public String getVersion() {
        return version;
    }

    public boolean isSuccess() {
        return success;
    }
}
