package be.haezebrouck.classtypeserialization;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class ResponseSerializationTest {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void serialize_deserialize() throws IOException {
        Response<String> original = new Response<>(
                String.class,
                "stringdata",
                42,
                true,
                "myversion");

        String serialized = objectMapper.writeValueAsString(original);

        System.out.println("serialized: " + serialized);

        Response<String> deserialized = objectMapper.readValue(serialized, new TypeReference<Response<String>>() {});

        assertThat(deserialized.getDataClass()).isEqualTo(original.getDataClass());

        assertThat(deserialized.getData()).isPresent().contains(original.getData().get());
        assertThat(deserialized.getVersion()).isEqualTo(original.getVersion());
        assertThat(deserialized.getExecutionDuration()).isEqualTo(original.getExecutionDuration());
        assertThat(deserialized.isSuccess()).isEqualTo(original.isSuccess());
    }
}
