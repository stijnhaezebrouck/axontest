package be.haezebrouck.axontest;

import be.haezebrouck.axontest.testaggregate.Create;
import be.haezebrouck.axontest.testaggregate.Update;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventhandling.tokenstore.TokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import javax.sql.DataSource;
import java.util.UUID;

import static java.util.concurrent.TimeUnit.SECONDS;

@SpringBootApplication
public class Run {

    @Autowired
    private CommandGateway commandGateway;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private TokenStore jdbcTokenStore;


    public static void main(String... args) {
        SpringApplication.run(Run.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        System.err.println("tokenstore: " + jdbcTokenStore.getClass().toString());

    }

    private void addAggregate(int size) {
        String id = UUID.randomUUID().toString();
        System.out.println(id+": adding "+size+" events...");

        commandGateway.sendAndWait(new Create(id), 30, SECONDS);
        commandGateway.sendAndWait(new Update(id, size), 30, SECONDS);
    }

}
