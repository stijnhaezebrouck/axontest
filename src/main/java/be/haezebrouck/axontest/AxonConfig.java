package be.haezebrouck.axontest;

import org.axonframework.config.Configurer;
import org.axonframework.eventhandling.async.FullConcurrencyPolicy;
import org.axonframework.eventhandling.tokenstore.jdbc.JdbcTokenStore;
import org.axonframework.eventsourcing.EventCountSnapshotTriggerDefinition;
import org.axonframework.eventsourcing.NoSnapshotTriggerDefinition;
import org.axonframework.eventsourcing.SnapshotTriggerDefinition;
import org.axonframework.eventsourcing.Snapshotter;
import org.axonframework.spring.eventsourcing.SpringAggregateSnapshotterFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AxonConfig {

    public static final String SNAPSHOT_TRIGGER_DEFINITION = "snapshotTriggerDefinition";

    @Value("${axon.snapshot.threshold}")
    private int axonSnapshotThreshold;

    public int snapshotThreshold() {
        return axonSnapshotThreshold;
    }

    @Autowired
    public void configureTokenStore(Configurer configurer, JdbcTokenStore tokenStore) {
        configurer.eventProcessing().registerTokenStore(c -> tokenStore);
    }

    @Bean
    public FullConcurrencyPolicy fullConcurrencyPolicy() {
        return new FullConcurrencyPolicy();
    }

    @Bean(SNAPSHOT_TRIGGER_DEFINITION)
    public SnapshotTriggerDefinition snapshotTriggerDefinition(Snapshotter snapshotter) {
        if (snapshotThreshold() <= 0) {
            return NoSnapshotTriggerDefinition.INSTANCE;
        }
        return new EventCountSnapshotTriggerDefinition(snapshotter, snapshotThreshold());
    }

    @Bean
    public SpringAggregateSnapshotterFactoryBean snapshotterFactoryBean() {
        return new SpringAggregateSnapshotterFactoryBean();
    }
}
