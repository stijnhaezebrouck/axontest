package be.haezebrouck.axontest;


import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZoneId;

import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

@Component
@ProcessingGroup("luisteraar")
public class Listener {

    private Logger log = LoggerFactory.getLogger(getClass());

    private long start = 0;
    private long counter = 0;

    @EventHandler
    public void on(Object event, @Timestamp Instant timestamp) {

        if (counter == 0) start = System.currentTimeMillis();
        counter++;

        if (counter % 10000L == 0) {
            long time = System.currentTimeMillis() - start;
            long speedPer10k = time / (counter / 10000);
            log.info(ISO_DATE_TIME.withZone(ZoneId.of("Europe/Brussels")).format(timestamp)+" "+ counter+ " events uitgelezen, 10k speed=" + speedPer10k + " time (ms): " + time);
        }
    }
}
