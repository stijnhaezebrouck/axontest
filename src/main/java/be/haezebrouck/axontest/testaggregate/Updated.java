package be.haezebrouck.axontest.testaggregate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Updated {

    @JsonProperty public final String id;
    @JsonProperty public final long eventNumber;

    @JsonCreator
    public Updated(@JsonProperty("id") String id, @JsonProperty("eventNumber") long eventNumber) {
        this.id = id;
        this.eventNumber = eventNumber;
    }
}
