package be.haezebrouck.axontest.testaggregate;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import static be.haezebrouck.axontest.AxonConfig.SNAPSHOT_TRIGGER_DEFINITION;
import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate (snapshotTriggerDefinition = SNAPSHOT_TRIGGER_DEFINITION)
public class TestAggregate {

    @JsonProperty
    @AggregateIdentifier
    private String id;

    @JsonProperty
    private int eventCounter = 0;

    public TestAggregate() {}

    @CommandHandler
    public TestAggregate(Create cmd) {
        apply(new Created(cmd.id));
    }

    @CommandHandler
    public void when(Update cmd) {
        for (long i = 0; i < cmd.numberOfEvents; i++) {
            apply(new Updated(cmd.id, i));
        }
    }

    @EventSourcingHandler
    private void on(Created created) {
        this.id = created.id;
        this.eventCounter++;
    }

    @EventSourcingHandler
    private void on(Updated udpated) {
        this.eventCounter++;
    }
}
