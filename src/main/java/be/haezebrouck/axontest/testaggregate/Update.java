package be.haezebrouck.axontest.testaggregate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

public class Update {


    @TargetAggregateIdentifier
    @JsonProperty public final String id;
    @JsonProperty public final long numberOfEvents;

    @JsonCreator
    public Update(@JsonProperty("id") String id, @JsonProperty("numberOfEvents") long numberOfEvents) {
        this.id = id;
        this.numberOfEvents = numberOfEvents;
    }
}
