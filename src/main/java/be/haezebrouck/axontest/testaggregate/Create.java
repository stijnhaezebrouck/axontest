package be.haezebrouck.axontest.testaggregate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

public class Create {

    @TargetAggregateIdentifier
    @JsonProperty
    public final String id;

    public Create() {
        this(UUID.randomUUID().toString());
    }

    @JsonCreator
    public Create(@JsonProperty("id") String id) {
        this.id = id;
    }

}
